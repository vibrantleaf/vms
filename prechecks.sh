#!/usr/bin/env bash
set -ouex pipefail
if [ -z ${SSHD_PORT+x} ]
then
 printf "Setting SSHD_PORT to the default: '22'\n"
 SSHD_PORT=22
else
printf "SSHD_PORT is set to '$SSHD_PORT'\n"
fi
if [ -z ${GIT_BRANCH+x} ]
then
 printf "Setting GIT_BRANCH to the default: 'current'\n"
 GIT_BRANCH=current
else
printf "GIT_BRANCH is set to '$GIT_BRANCH'\n"
fi
if [ -z ${PET+x} ]
then
 printf "Setting PET to the default: 'sudo'\n"
 PET=sudo
else
 printf "PET is set to '$PET'\n"
fi
function uefiCheck {
  printf "Checking your system is Booting in uEFI mode\n"
  if [ -d "/sys/firmware/efi" ]
  then
    printf "Host os booted in uEFI mode\n"
  else
    printf "ERROR: Your Host Install is not booted in uEFI mode,\n\
    Please try to boot your Host-Device in uEFI mode or reinstall your Host-OS.\n\
    If your Host-device does not support uEFI, You may not conntinue\n"
  fi
}
function secbootCheck {
  printf "Checking your system has Secure Boot Enabled\n\
  Please enter your $PET password so this function will work\n"
  $PET printf "thank you\n" 
  if $PET mokutil --sb-state | grep "SecureBoot enabled"
  then
    printf "SecureBoot is enabled\n"
  elif $PET mokutil --sb-state | grep "Failed to read SecureBoot"
  then
    printf "ERROR: Secure Boot is not enabled BAD\n Please setup Secure Boot on your system"
  else
    printf "ERROR: Could not verify if Secure Boot is enabled or not, Please fix this."
  fi
}
function virtCheck {
  printf "Checking your system has Virtualization Enabled\n"
  if ((  $(lscpu | grep -i "Virtualization" | wc -l) > 0 ))
  then
    printf "Virtualization is Enabled  \n"
  else
    printf "ERROR: Virtualization is NOT Enabled BAD\n\
    if your CPU supportes Virtualization\n\
    please enable Virtualization support in your UEFI-BIOS\n"
  fi 
}
function zypperPkgs {
  printf "Installing needed zypper packages\n\
  Please enter your $PET password so this function will work\n"
  $PET printf "thank you\n"
  $PET zypper ref
  $PET zypper up
  $PET zypper dup
  $PET zypper in $(curl https://codeberg.org/vibrantleaf/vms/raw/branch/$GIT_BRANCH/needed_zypper_packages_list.txt)
}
function setModprobes {
  printf "Enabling nested Virtualization Modprobes\n\
  Please enter your $PET password so this function will work\n"
  $PET printf "thank you\n" 
  if [ $(cat /proc/cpuinfo | grep "AuthenticAMD" | wc -l ) > 0 ]
  then
    printf "Your CPU's vendor was automaticly detected as 'AuthenticAMD'\n"
    $PET modprobe -r kvm_amd kvm
    $PET modprobe kvm
    $PET modprobe kvm_amd nested=1
    printf "restarting libvirtd service\n"
    $PET systemctl restart libvirtd.service
  elif [ $(cat /proc/cpuinfo | grep "GenuineIntel" | wc -l ) > 0 ]
  then
    printf "Your CPU's vendor was automaticly detected as 'GenuineIntel'\n"
    $PET modprobe -r kvm_intel kvm
    $PET modprobe kvm
    $PET modprobe kvm_intel nested=1
    printf "Restarting libvirtd service\n"
    $PET systemctl restart libvirtd.service
  else
   printf "ERROR: Unable to automaticlty detect your CPU vender as 'AuthenticAMD' or 'GenuineIntel\n'"
  fi
}
function services {
  printf "Enabling tuned & zramswap services\n\
  Please enter your $PET password so this function will work\n $($PET printf 'thank you') \n
  Enabling Tuned\n"
  $PET systemctl enable --now tuned.service
  printf "Seting Default Tuned Config\n"
  $PET tuned-adm profile balanced
  printf "Enabling zram\n for more info see: https://wiki.archlinux.org/title/Zram\n"
  $PET systemctl enable --now zramswap.service
}
function firewall {
  printf "Allowing sshd connection in firewalld \n\
  Please enter your $PET password so this function will work\n $($PET printf 'thank you') \n" 
  $PET firewall-cmd --permanent --zone=public --add-port=$SSHD_PORT/tcp
  $PET firewall-cmd --reload
}
function restartApps {
  printf "Restarting virt-manager processes\n"
  pkill virt-viewer
  pkill virt-manager
  virt-manager & > /dev/null
}
function modularLibvirt {
  printf "Switching from Monolithic libvirtd daemons to modular libvirtd daemons\n\
  Please enter your $PET password so this function will work\n $($PET printf 'thank you') \n\ 
  Disabling monolithic libvirtd daemons\n"
  $PET systemctl disable --now libvirtd.service
  $PET systemctl disable --now libvirtd{,-ro,-admin}.socket
  printf "Enabling modular libvirtd daemons\n"
  for drv in qemu network nodedev nwfilter secret storage proxy
  do
    $PET systemctl enable --now virt${drv}d.service
    $PET systemctl enable --now virt${drv}d{,-ro,-admin}.socket
  done
}
function printVersions {
   printf "Please enter your $PET password so this function will work\n $($PET printf 'thank you') \n\
   Here are the Currnet versions for Your Virtualization Stack:\n\
     Linux Kernel version: '$(uname -r)'\n\
     QEMU version is: '$(qemu-kvm --version | grep version | cut -d ' ' -f 4-)'\n\
     LibVirt version is: '$(virsh --version)'\n\
     Virt-Manager version is: '$(virt-manager --version)'\n\
     Systemd version is: '$(systemctl --version | grep systemd | cut -d ' ' -f 2- )'\n\
     Tuned version is: '$($PET tuned-adm --version | cut -d ' ' -f 2)'\n\
     Zram version is: '$($PET zramctl --version | cut -d ' ' -f 4)'\n\
     Bash version is: '$(bash --version | grep bash | cut -d ' ' -f 1,4)'\n"
}
function exec {
  uefiCheck
  secbootCheck
  virtCheck
  zypperPkgs
  printVersions
  killApps
  firewall
  setModprobes
  modularLibvirt
  restartApps
}
function version {
  printf "prechecks.sh version: 6.i \n"
}
function help {
  printf "prechecks.sh\n\
  Usage: [OPTIONS] ./prechecks.sh [COMMAND]\n\
   \n\
   ['COMMAND': DESCRIPTION]\n\
   \n\
    'version'       : Print the prechecks.sh version\n\
    'exec'          : Executie all setps\n\
    'uefiCheck'     : Checks if your system is running in UEFI mode\n\
    'secbootCheck'  : Checks if your system is using secureboot\n\
    'virtCheck'     : Checks if your system has Hardware assested Virtualisation support enabled\n\
    'printVersions' : Prints the Currnet versions for Your Virtualization Stack\n\
    'zypperPkgs'    : install needed zypper packages\n\
    'setModprobes'  : automaticly enables nested kvm for AMD & INTEL CPUs\n\
    'firewall'      : uses firewalld to allow ssh access though port '$SSHD_PORT'\n\
    'modularLibvirt': Switches from monolithic libvirtd daemons to modular Libvirtd Daemons\n\
    'restartApps'   : restart virt-manager\n\
  \n\
  OPTIONS:\n\
   ['OPTIONS' : DESCRIPTION]\n\
    'SSHD_PORT'     : Optionaly Change the default sshd port, Default is '22'\n\
    'GIT_BRANCH'    : Optionaly Change the default git branch, Default is 'current'\n\
    'PET'           : Optionaly Change the default Privilege Escalation Tool, Default is 'sudo'\n\
 \n\
  EXAPLES:\n\
   './prechecks.sh exec'                       : Exec all steps with the default options\n\
   'PET=doas SSHD_PORT=69 ./prechecks.sh exec' : Exec all steps with the some custom options\n"
}
function main {
  help
}
if [ -n "$1" ]
then
  $1
else
  main
fi