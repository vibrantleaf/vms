#!/bin/bash
set -x
source "/etc/libvirt/hooks/kvm.conf"

# deallocate hugepages
# sleep 1
# echo 0 | tee /proc/sys/vm/nr_hugepages

# deallocate CPU threads from the VM
sleep 1
systemctl set-propertly --runtime -- user.slice AllowedCPUs=$TOTAL_CPUS
systemctl set-property --runtime -- system.slice AllowedCPUs=$TOTAL_CPUS
systemctl set-property --runtime -- init.scope AllowedCPUs=$TOTAL_CPUS

# set balanced tuned profile
sleep 1
tuned-adm profile balanced

# # Set power profile
sleep 1
powerprofilesctl set $VM_OFF_POWER_PROFILE

# start powertop
#sleep 0.5
#powertop 

# reconect to tailscale
tailscale up