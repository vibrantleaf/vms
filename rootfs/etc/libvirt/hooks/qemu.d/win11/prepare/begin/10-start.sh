#!/bin/bash
set -x
source "/etc/libvirt/hooks/kvm.conf"

#clear memory caches
#echo "3" | tee /proc/sys/vm/drop_caches

# defrag ram
#echo "1" | tee /proc/sys/vm/compact_memory

# trigger insomnia 
systemctl start libvirt-insomnia@win11

# stop display manager
systemctl stop gdm.service

# sset virtuakl host tuned profile
tuned-adm profile virtual-host

# Disconnect from Tailscale
tailscale down

# start sshd
#systemctl start sshd.service

# # start samba
# systemctl start smbd.service

# prepare for passthrough
sleep 2
for file in /sys/class/vtconsole/vtcon*/bind
do
  echo 0 | tee $file
done

sleep 1
echo efi-framebuffer.0 | tee /sys/bus/platform/drivers/efi-framebuffer/unbind

# detach gpu
sleep 10
modprobe -r amdgpu

# setup resizeable bar
echo 14 | tee /sys/bus/pci/devices/$VIRSH_GPU_VIDEO_BAR/resource0_resize
sleep 1

echo 3 | tee /sys/bus/pci/devices/$VIRSH_GPU_VIDEO_BAR/resource2_resize
sleep 2

# detach gpu
virsh nodedev-detach $VIRSH_GPU_VIDEO
virsh nodedev-detach $VIRSH_GPU_AUDIO

# load vfio drivers
sleep 10
modprobe vfio
modprobe vfio-pci
modprobe vfio_iommu_type1
modprobe vfio_virqfd

# # Set power profile
sleep 1
powerprofilesctl set $VM_ON_POWER_PROFILE

# allocate CPU threads to the VM
sleep 2
systemctl set-propertly --runtime -- user.slice AllowedCPUs=$SYSTEM_CPUS
systemctl set-property --runtime -- system.slice AllowedCPUs=$SYSTEM_CPUS
systemctl set-property --runtime -- init.scope AllowedCPUs=$SYSTEM_CPUS

#clear memory caches
echo "3" | tee /proc/sys/vm/drop_caches