#!/bin/bash
set -x
source "/etc/libvirt/hooks/kvm.conf"

# deallocate CPU threads from the VM
sleep 1
systemctl set-propertly --runtime -- user.slice AllowedCPUs=$TOTAL_CPUS
systemctl set-property --runtime -- system.slice AllowedCPUs=$TOTAL_CPUS
systemctl set-property --runtime -- init.scope AllowedCPUs=$TOTAL_CPUS

# Set power profile
sleep 1
powerprofilesctl set $VM_OFF_POWER_PROFILE

#Reattach GPU to the host
sleep 1
virsh nodedev-reattach $VIRSH_GPU_VIDEO 
virsh nodedev-reattach $VIRSH_GPU_AUDIO

# unload the VFIO drivers
sleep 10
modprobe -r vfio
modprobe -r vfio-pci
modprobe -r vfio_iommu_type1
modprobe -r vfio_virqfd

# prepare for returning the GPU
sleep 3
echo "efi-framebuffer.0" | tee /sys/bus/platform/drivers/efi-framebuffer/bind

# reload GPU drivers
sleep 3
modprobe amdgpu

# rebind the vtcons
sleep 1
for file in /sys/class/vtconsole/vtcon*/bind
do
  echo 1 | tee $file
done
  
# stop samba
# systemctl stop smb.service
 
# stop sshd
#systemctl stop sshd.service

# set balanced tuned profile
sleep 1
tuned-adm profile balanced
 
# Reconect to tailscale
tailscale up

# restart the display manager
sleep 1
systemctl start gdm.service

#clear memory caches
echo "3" | tee /proc/sys/vm/drop_caches

# disable insomnia
sleep 1
systemctl stop libvirt-insomnia@win11