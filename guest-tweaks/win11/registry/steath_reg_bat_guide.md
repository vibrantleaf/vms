1. copy `steath_reg.bat` & `registry_hacks.reg` to `C:\steath_reg.bat` & `C:\registry_hacks.reg`
2. edit `C:\registry_hacks.reg` so that `"FriendlyName"="SABRENT ROCKET 1TB 4.0 NVMe"` matches the physical disc that your virtual disc is located on
3. Right-click on `C:\steath_reg.bat` , 'Show more Options' , 'Create shortcut'
4. Right-click on `C:\Users\Your_User\Desktop\steath_reg.bat.lnk` , 'Properties' , 'Advanced...' , check 'Run as administrator'
5. move `C:\Users\Your_User\Desktop\steath_reg.bat.lnk` to `%appdata%\Microsoft\Windows\Start Menu\Programs\Startup\steath_reg.bat`

this will automaticly apply some "registry hacks" to help hide that windows 11 is running inside a virtual machine

this will fix `SystemBiosVersion` being flaged in pafish and will help hide some virtual machine devices in device manager 